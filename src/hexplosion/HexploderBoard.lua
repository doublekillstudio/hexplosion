---------------------------------------------------------------------------------------------------
-- Hexploder Board Module --
---------------------------------------------------------------------------------------------------

local HexploderBoard = {}
math.randomseed (os.time ())


---------------------------------------------------------------------------------------------------
-- Utils --
---------------------------------------------------------------------------------------------------

HexploderBoard.printf = function (s, ...)
	return io.write (s:format (...))
end

---------------------------------------------------------------------------------------------------

HexploderBoard.In = function (val, table)
	if val == nil or table == nil or type (table) ~= 'table' then
		return false
	end

	for i, v in pairs (table) do
		if v == val then
			return true
		end
	end

	return false
end


---------------------------------------------------------------------------------------------------
-- Location --
---------------------------------------------------------------------------------------------------

HexploderBoard.Location = {}

HexploderBoard.Location.New = function (x, y)
	return {x = x, y = y}
end


---------------------------------------------------------------------------------------------------
-- Board --
---------------------------------------------------------------------------------------------------

HexploderBoard.New = function (width, height, src_board)
	local hb  = {}
	hb.width  = width
	hb.height = height
	hb.board  = HexploderBoard.InitBoard (width, height, src_board)
	return hb
end

---------------------------------------------------------------------------------------------------

HexploderBoard.InitBoard = function (width, height, src_board)
	local board = {}
	
	for y = 1, height do
		board[y] = {}
		for x = 1, width do
			board[y][x] = (src_board and src_board[y] and src_board[y][x]) and src_board[y][x] or '.'
		end
	end

	if src_board ~= nil then
		return board
	end

	for y = 1, height / 2 do
		for x = 1, height / 2 - y + 1 do
			board[y][width - x + 1]  = 'x'
			board[height - y + 1][x] = 'x'
		end
	end

	return board
end

---------------------------------------------------------------------------------------------------

HexploderBoard.Show = function (hb)
	local max_invalids = 0
	local invalids = {}

	for y = 1, hb.height do
		invalids[y] = 0
		for x = 1, hb.width do
			if (hb.board[y][x] == 'x') then
				invalids[y] = invalids[y] + 1
			end
		end
		max_invalids = max_invalids + invalids[y]
	end

	local identation = ''
	for i = 1, max_invalids do
		identation = identation .. ' '
	end

	for y = 1, hb.height do
		HexploderBoard.printf (string.sub (identation, 1, invalids[y]))
		for x = 1, hb.width do
			if hb.board[y][x] ~= 'x' then
				HexploderBoard.printf (hb.board[y][x] .. ' ')
			end
		end
		HexploderBoard.printf ("\n")
	end

	HexploderBoard.printf ("--------------------------------------------\n")
end

-----------------------------------------------------------------------------------------------------

HexploderBoard.LocationsGet = function (hb, val, max)
	max = max or 0
	local locations = {}
	for y = 1, hb.height do
		for x = 1, hb.width do
			if (hb.board[y][x] == val) then
				table.insert (locations, HexploderBoard.Location.New (x, y))

				if (max > 0 and #locations >= max) then
					return locations
				end
			end
		end
	end
	return locations
end

-----------------------------------------------------------------------------------------------------

HexploderBoard.NeighboursGet = function (hb, origin, valid_neighbours)
--[[  [N][N][ ]
      [N][x][N]
      [ ][N][N]  ]]
	local neighbours = {}
	
	for l = -1, 1 do
		for c = -1, 1 do
			--[[Ignores the center and one of the diagonals. Also, avoid selecting neighbours from
			    outside the board]]
			if c + l ~= 0 and origin.x + c >= 1 and origin.x + c <= hb.width and
					origin.y + l >= 1 and origin.y + l <= hb.height and
					HexploderBoard.In (hb.board[origin.y + l][origin.x + c], valid_neighbours) == true then
				table.insert (neighbours, HexploderBoard.Location.New (origin.x + c, origin.y + l))
			end
		end
	end
	return neighbours
end

---------------------------------------------------------------------------------------------------

HexploderBoard.RegionMark = function (hb, location, valid_neighbours)
	local neighbours = HexploderBoard.NeighboursGet (hb, location, valid_neighbours)
	hb.board[location.y][location.x] = 'M'

	for i, n in pairs (neighbours) do
		if hb.board[n.y][n.x] == 0 then
			HexploderBoard.RegionMark (hb, n, valid_neighbours)
		else
			hb.board[n.y][n.x] = 'M'
		end
	end
end

---------------------------------------------------------------------------------------------------

HexploderBoard.RegionGet = function (hb, location)
	local temp = HexploderBoard.New (hb.width, hb.height, hb.board)
	HexploderBoard.RegionMark (temp, location, {0, 1, 2, 3, 4, 5, 6})

	local region = {}
	for y = 1, temp.height do
		for x = 1, temp.width do
			if temp.board[y][x] == 'M' then
				table.insert (region, HexploderBoard.Location.New (x, y))
			end
		end
	end

	return region
end

---------------------------------------------------------------------------------------------------

HexploderBoard.GenerateBombs = function (hb, bombs)
	local locations = nil
	local bomb_pos = nil
	while bombs > 0 do
		local locations = HexploderBoard.LocationsGet (hb, '.')
		bomb_pos = locations[math.random (#locations)]
		hb.board[bomb_pos.y][bomb_pos.x] = 'b'
		bombs = bombs - 1
	end
	HexploderBoard.GenerateMarks (hb)
end

---------------------------------------------------------------------------------------------------

HexploderBoard.GenerateMarks = function (hb)
	local neighbours
	for y = 1, hb.height do
		for x = 1, hb.width do
			if hb.board[y][x] == '.' then
				neighbours = HexploderBoard.NeighboursGet (hb, HexploderBoard.Location.New (x, y), {'b'})
				hb.board[y][x] = #neighbours
			end
		end
	end
end

---------------------------------------------------------------------------------------------------

return HexploderBoard
