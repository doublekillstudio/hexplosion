application = {
	content = {
		width = 320,
		height = 480, 
		scale = "letterBox",
		fps = 30,
		
        imageSuffix = {
		    ["@2x"] = 1.5,
		    ["@4x"] = 3,
			["@8x"] = 6,
		}
	},

    --[[
    -- Push notifications

    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert", "newsstand"
            }
        }
    }
    --]]    
}