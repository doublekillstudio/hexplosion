-------------------------------------------------------------------------------
-- main.lua
--
-------------------------------------------------------------------------------

-- requires
local game = require( "scripts.game" )
local storyboard = require( "storyboard" )
local colors = require( "scripts.colors" )
local log = require( "scripts.log" )

-- game scenes modules
require( "scripts.scenes.menuscene" )
require( "scripts.scenes.tutorialscene" )
require( "scripts.scenes.difficulties" )
require( "scripts.scenes.gamescene" )

---------------------------------------
do -- handle back button
	local function onKeyEvent( event )

		local phase = event.phase
		local key = event.keyName
		log.debug( '%s %s', event.phase, event.keyName )

		if "back" == key and "up" == phase then
			local scene = storyboard.getCurrentSceneName()
			if scene ~= "menuscene" then
				storyboard.gotoScene( 'menuscene', {
					effect = "slideRight",
					time = "250"
				} )
				return true
			end
        end
		
    end
	
	Runtime:addEventListener( "key", onKeyEvent )
end
---------------------------------------
do -- storyboard
	
	-- setup some global configs
	display.setStatusBar (display.HiddenStatusBar)
	display.setDefault ("background", unpack (colors.background))
--
	local bg = display.newImageRect ('images/background.png',
		display.pixelWidth * display.contentScaleX, -- for 4/3 screen ratio
		display.pixelHeight * display.contentScaleY -- for 16/9 screen ratio
		)
	bg.x = display.contentCenterX
	bg.y = display.contentCenterY
	bg:toBack ()

	-- start first scene
	
	local HALF_SQRT3 = math.sqrt(3)/2
	local width = display.contentWidth * 0.2
	local height = width / ( math.sqrt(3) / 2 )
	
	
	local sizes = { 32, 64, 128 }
	for i = 1, #sizes do
		local height = sizes[i]
		local width = height * ( math.sqrt(3) / 2 )
		log.debug( "%dx%d", width, height )
	end
	--[[
	27.712812921102 ->  28 x  32
	55.425625842204 ->  55 x  64
	110.85125168441 -> 111 x 128
	]]
	
	storyboard.gotoScene( 'menuscene' )
end
