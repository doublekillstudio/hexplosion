-------------------------------------------------------------------------------
--
-- menuscene.lua
--
-------------------------------------------------------------------------------

-- requires
local adsPlugin = require ("ads")
local log = require ("scripts.log")

local ads = {}
do
	ads.interstitial_percentage = 10 -- A 10% chance to show an interstitial ad each time the user exits the game
	ads.interstitial_ignore     = 2  -- Garantee interstitial ads are not show the first N times the user exits the game
	ads.interstitial_count      = 0

	log.debug ('[Ads] Loading')


	--*********************************************************************************************
	--** Vungle **
	--*********************************************************************************************

	ads.vungle = {
		-- This is app-dependent
		keys = {
			["Android"] = "533da801f3ae8123380000af",
			["iPhone OS"] = "533da78b1ef1d3223800007a"
		},
		retryDelay = 3000,
		retries    = 5,
	}

	-----------------------------------------------------------------------------------------------

	ads.vungle.init = function ()
		log.debug ('[Vungle] Initializing')
		ads.vungle.key = ads.vungle.keys[system.getInfo ("platformName")]
		adsPlugin.init ('vungle', ads.vungle.key, ads.vungle.listener)
	end

	-----------------------------------------------------------------------------------------------

	ads.vungle.show = function ()
		log.debug ('[Vungle] Trying to show an ad')
		adsPlugin.show (ads.currentType, {isBackButtonEnabled = true})
	end

	-----------------------------------------------------------------------------------------------

	ads.vungle.listener = function (event)
		log.debug ('[Vungle] Received an event')
		if event.type == "adStart" and event.isError then
			log.debug ("[Vungle] Error showing ad")

			ads.tries = ads.tries + 1
			if ads.tries > ads.vungle.retries then
				ads.hide ()
				ads.show ('banner')
			else
				log.debug ('[Vungle] Trying again')
				ads.timer = timer.performWithDelay (ads.vungle.retryDelay, ads.vungle.show)
			end
		elseif event.type == "adEnd" then
			log.debug ('[Vungle] Ad finished')
		else
			log.debug ('[Vungle] Unknown event')
		end
	end


	--*********************************************************************************************
	--** AdMob **
	--*********************************************************************************************

	ads.admob = {
		-- This is app-dependent
		keys = {
			["Android"] = "ca-app-pub-4874679093389982/4324818759",
			["iPhone OS"] = "ca-app-pub-4874679093389982/1503412353",
		},
		retryDelay = 1000,
		retries    = 3,
	}

	-----------------------------------------------------------------------------------------------

	ads.admob.listener = function (event)
		log.debug ('[AdMob] Received an event')
		if event.isError then
			log.debug ("[AdMob] Error showing ad")

			ads.tries = ads.tries + 1
			if ads.tries > ads.admob.retries then
				ads.show (ads.currentType, ads.findNextProvider (ads.currentType, 'admob'))
			else
				log.debug ('[AdMob] Trying again')
				ads.timer = timer.performWithDelay (ads.admob.retryDelay, ads.admob.show)
			end
		else
			log.debug ("[AdMob] Ad successfully shown")
		end
	end

	-----------------------------------------------------------------------------------------------

	ads.admob.show = function ()
		log.debug ('[AdMob] Trying to show an ad')
		if system.getInfo ("environment") == 'simulator' then
			-- banner size in pixels
			-- 50x320 is the proportionally higher banner for phone screens
			local bannerPixelHeight = display.pixelWidth * 50/320
			if display.pixelHeight / display.pixelWidth < 1.49 then
				-- 90x768 is the reference banner for ipad, it should work
				bannerPixelHeight = display.pixelWidth * 90/768
			end

			local bannerPixelWidth = display.pixelWidth
			local bannerContentWidth = bannerPixelWidth * display.contentScaleX
			local bannerContentHeight = bannerPixelHeight * display.contentScaleY

			-- difference of actual screen vs reference screen
			local errorY = (display.pixelHeight * display.contentScaleY) - display.contentHeight

			local bannerPosY = display.contentHeight - bannerContentHeight + errorY/2

			local simAdd = display.newRect (
					display.screenOriginX, -- x
					bannerPosY, -- y
					bannerContentWidth, -- width
					bannerContentHeight -- height
				)
			simAdd.anchorX = 0
			simAdd.anchorY = 0
			simAdd:setFillColor (0)
			ads.simulatedAd = simAdd
		else
			local xPosition = 0
			local yPosition = 0
			if ads.currentType == 'banner' then
				xPosition = display.screenOriginX
				-- Workaround to properly position the bottom ad
				-- http://forums.coronalabs.com/topic/41351-keeping-admob-to-the-bottom-of-screen-across-platform/
				yPosition = 10000
			end

			adsPlugin.show (ads.currentType, {x = xPosition, y = yPosition, testMode = false, appId = ads.admob.key})
		end
	end

	-----------------------------------------------------------------------------------------------

	ads.admob.init = function  ()
		log.debug ('[AdMob] Initializing')
		ads.admob.key = ads.admob.keys[system.getInfo ("platformName")]
		adsPlugin.init ("admob", ads.admob.key, ads.admob.listener)
	end


	--*********************************************************************************************
	--** Ads Management **
	--*********************************************************************************************

	ads.currentProvider = nil
	ads.currentType     = nil
	ads.timer           = nil

	-- TODO: This is platform-dependent
	ads.supportedTypes = {
		'banner', 'interstitial', 'incentivized',
		banner       = {'admob',
		                 admob = true},
		interstitial = {'vungle', 'admob',
		                 vungle = true, admob = true},
		incentivized = {'vungle',
		                 vungle = true},
	}

	-----------------------------------------------------------------------------------------------

	ads.init = function ()
		log.debug ('[Ads] Initializing all providers')
		-- TODO: this is not generic enough
		ads.admob.init ()
		ads.vungle.init ()
	end

	-----------------------------------------------------------------------------------------------

	ads.show = function (type, provider)
		log.debug ('[Ads] Showing an ad')

		type = type ~= nil and type or ads.supportedTypes[1]
		if ads.supportedTypes[type] == nil then
			log.debug ("[Ads] Error: Unsuportted type '" .. type .. "'")
			return
		end

		if provider ~= nil then
			if ads.supportedTypes[type][provider] == nil then
				log.debug ("[Ads] Error: Unsuportted type '" .. type .. "' to provider '" .. provider .. "'")
				return
			end
		else
			provider = ads.findNextProvider (type)
		end

		if ads.currentProvider ~= provider then
			adsPlugin:setCurrentProvider (provider)
			ads.currentProvider = provider
		end

		ads.tries = 0
		ads.currentType = type
		ads[provider].show ()
	end

	-----------------------------------------------------------------------------------------------

	ads.hide = function ()
		log.debug ('[Ads] Hiding add')
		if ads.timer ~= nil then
			timer.cancel (ads.timer)
			ads.timer = nil
		end

		if system.getInfo ("environment") == 'simulator' then
			if ads.simulatedAd then
				ads.simulatedAd:removeSelf ()
				ads.simulatedAd = nil
			end
		else
			adsPlugin.hide ()
		end
	end

	-- ............................................................................................

	ads.findNextProvider = function (type, provider)
		local next = {}

		if provider == nil then
			next.banner       = ads.supportedTypes.banner[1]
			next.interstitial = ads.supportedTypes.interstitial[1]
			next.incentivized = ads.supportedTypes.incentivized[1]
			return next[type]
		end

		if type == 'banner' then
			next.admob = 'admob'
		elseif type == 'interstitial' then
			next.admob = 'vungle'
			next.vungle = 'admob'
		elseif type == 'incentivized' then
			next.vungle = 'vungle'
		end
		return next[provider]
	end

	-- ............................................................................................

	ads.canShowInterstititalAd = function ()
		ads.interstitial_count = ads.interstitial_count + 1
		if ads.interstitial_count <= ads.interstitial_ignore then
			return false
		end
		return math.random (100 / ads.interstitial_percentage) == 1
	end


	--*********************************************************************************************
	--** Initialization **
	--*********************************************************************************************

	ads.init ()

	log.debug ('[Ads] Loaded')

end
return ads
