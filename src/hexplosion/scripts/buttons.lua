-------------------------------------------------------------------------------
--
-- buttons.lua
--
-------------------------------------------------------------------------------

local colors = require( "scripts.colors" )

local buttons = {}
do
	function buttons.createButton( opts )
		local group = display.newGroup() 
		group.x = opts.x
		group.y = opts.y
		
		local rect = display.newRect(
			group, 0, 0, -- parent, x, y
			opts.width, 
			opts.height
		)
		rect:setFillColor( unpack(colors.button) )
		rect:setStrokeColor( 0 )
		rect.strokeWidth = 4
		
		local text = display.newText{
			parent = group,
			text = opts.label,
			font = native.systemFontBold,
			fontSize = 32
		}
		
		if opts.onTap then
			group:addEventListener( "tap", opts.onTap )
		end
		
		if opts.parent then
			opts.parent:insert( group )
		end
		return group
	end
	
	function buttons.createImageButton( opts )
		local group = display.newGroup()
		group.x = opts.x or 0
		group.y = opts.y or 0
		
		local shadow = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		shadow.x, shadow.y = 1, 3 -- i, i
		shadow:setFillColor( 0, 0, 0 )
		shadow:toBack()
		shadow.alpha = 0.25
		
		local image = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		
		if opts.onTap then
			group:addEventListener( "tap", opts.onTap )
		end
		
		if opts.parent then
			opts.parent:insert( group )
		end
		
		-- hack
		function group:setFillColor(...)
			image:setFillColor(...)
		end
		
		return group
	end
end
return buttons