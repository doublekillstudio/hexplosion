-------------------------------------------------------------------------------
--
-- colors.lua
--
-------------------------------------------------------------------------------

local colors = {}
do
	-- local functions
	
	local function fromHex( hexCode )
		assert(#hexCode == 7, "The hex value must be passed in the form of #XXXXXXXX");
		local hexCode = hexCode:gsub("#","")
		return tonumber("0x"..hexCode:sub(1,2))/255,tonumber("0x"..hexCode:sub(3,4))/255,tonumber("0x"..hexCode:sub(5,6))/255
	end
	
	-- colors
	local palette =  {
		yellow  =  { fromHex( '#ffff00' ) },
		yellow2 =  { fromHex( '#8c6b00' ) },
		red     =  { fromHex( '#ff0000' ) },
		darkred =  { fromHex( '#800000' ) },
		gray80  =  { fromHex( '#333333' ) },
		black   =  { fromHex( '#000000' ) },
		gray30  =  { fromHex( '#b3b3b3' ) },
		gray40  =  { fromHex( '#999999' ) },
		gray025 =  { fromHex( '#f9f9f9' ) },
		red_2   =  { fromHex( '#2b0000' ) },
		red_3   =  { fromHex( '#550000' ) },
		red_4   =  { fromHex( '#800000' ) },
		red_5   =  { fromHex( '#aa0000' ) },
		red_6   =  { fromHex( '#d40000' ) },
	}
	
	colors.background  = { fromHex( '#eeeeee' ) } -- palette.gray025
	colors.button      = palette.gray80
	colors.buttonLight = palette.gray025
	colors.buttonLabel = palette.gray025
	colors.transparent      = { 1, 0 }
	colors.shadows 	   = palette.black
	
	colors.bottom_bar = {
		fill = palette.black,
		text = palette.yellow,
		textShadow = palette.yellow2
	}
	
	colors.hidden = {
		fill = palette.gray80
	}
	colors.number = {
		fill = palette.gray40,
		fillZero = palette.gray30,
		text1 = palette.black,
		text2 = palette.red_2,
		text3 = palette.red_3,
		text4 = palette.red_4,
		text5 = palette.red_5,
		text6 = palette.red_6
	}
	
	-- 0.85, 0.90, 1 } -- {0.94, 0.96, 1} {1, 1, 1} {0.85, 0.90, 1} {0.9, 0.92, 1}
	--colors.button = { 0.3, 0.5, 0.8 }0.2, 0.392, 0.682
	--colors.button = { 0.2, 0.392, 0.682 }
	--colors.buttonLight = { 0.45, 0.65, 0.95 }
	--colors.buttonLabel = { 0.85, 0.90, 1 }
end
return colors