-------------------------------------------------------------------------------
--
-- game.lua
--
-------------------------------------------------------------------------------

local widget = require( "widget" )
local storyboard = require( "storyboard" )

local hexploderBoard = require( "HexploderBoard" )
local buttons = require( "scripts.buttons" )
local colors = require( "scripts.colors" )

---------------------------------------
-- parameters for difficulties
local difficultyParams = {
	easy   = { width = 8,  height=9,  bombs=9 },  --   9/52 ~ 0.17
	medium = { width = 10, height=11, bombs=15 }, --  15/80 ~ 0.19
	medium = { width = 10, height=11, bombs=15 }, --  15/80 ~ 0.19
	hard   = { width = 15, height=17, bombs=40 }  -- 40/183 ~ 0.22
}

---------------------------------------
-- utility class for hex conversions
local HHexMath = {}
do
	-- consts
	local HALF_SQRT3 = math.sqrt(3)/2
	
	function HHexMath.toCartesian(xh, yh)
		return xh - yh/2, yh*HALF_SQRT3
	end

	function HHexMath.fromCartesian(xc, yc)
		local yh = yc/HALF_SQRT3
		return xc + yh/2, yh
	end
end

-- game data
local difficulty = 'medium'
local templateBoard = {}
local hexes = {}
local selectedHex = nil
local flagMode = false
local state = nil
local gameTime = 0

-- the game class
local GAME

---------------------------------------
local DRAW = {}
do -- DRAW
	
	-- constants
	local screenW, screenH = display.contentWidth, display.contentHeight
	local centerX, centerY = screenW*0.5, screenH*0.5
	local HALF_SQRT3 = math.sqrt(3)/2
	
	local params = {}
	do 
		params.easy = {
			origin = { x=85, y=70 },
			scale = 38
		}
		params.medium = {
			origin = { x=80, y=70 },
			scale = 32
		}
		params.hard = {
			origin ={ x=87, y=72 },
			scale = 21
		}
	end
	
	-- private vars
	local viewGroup = nil
		
	--  public vars
	DRAW.scale = params.medium.scale
	DRAW.origin = params.medium.origin
	
	-- private functions
	local function cartesianToScreen(xc, yc, screenOrigin, scale)
		return screenOrigin.x + xc*scale, screenOrigin.y + yc*scale
	end
	
	local function screenToCartesian(xs, ys, screenOrigin, scale)
		return (xs - screenOrigin.x)/scale, (ys - screenOrigin.y)/scale
	end
	
	local function hexToScreen(xh, yh, screenOrigin, scale)
		local xc, yc = HHexMath.toCartesian(xh, yh)
		return cartesianToScreen(xc, yc, screenOrigin, scale)
	end
	
	local function screenToHex(xs, ys, screenOrigin, scale)
		local xc, yc = screenToCartesian(xs, ys, screenOrigin, scale)
		return HHexMath.fromCartesian(xc, yc)
	end
	
	local function viewHexShadow( x, y, width )
		width = width*0.95
		local height = width / HALF_SQRT3
		local hex = display.newImageRect( "images/hex.png", width, height )
		hex:setFillColor( unpack( colors.shadows ) )
		hex.x, hex.y = x, y + width * 0.07
		hex.alpha = 0.5
		return hex
	end
	
	local function viewHidden(x, y, width)
		
		local group = display.newGroup()
		group.x, group.y = x, y
	
		local shadow = viewHexShadow( 0, 0, width )
		group:insert( shadow )
	
		local height = width / HALF_SQRT3
		local hex = display.newImageRect( group, "images/hex.png", width, height )
		hex:setFillColor( unpack( colors.hidden.fill ) )
		
		return group
	end
	
	local function viewNumber(x, y, width, value)
	
		local group = display.newGroup()
		group.x, group.y = x, y
	
		local shadow = viewHexShadow( 0, 0, width )
		group:insert( shadow )
	
		local height = width / HALF_SQRT3
		local hex = display.newImageRect( group, "images/hex.png", width, height )
		
		if value == 0 then
			hex:setFillColor( unpack( colors.number.fillZero ) )
		else
			hex:setFillColor( unpack( colors.number.fill) )
			if value == 1 then
			elseif value == 2 then
			elseif value == 3 then
			elseif value == 4 then
			elseif value == 5 then
			elseif value == 6 then
			end
			local number = display.newText{
				parent = group,
				text = value,
				font = native.systemFont,
				fontSize = width / 2
			}
			local colorName = 'text' .. tostring(value)
			number:setFillColor( unpack( colors.number[colorName] ) )
		end
		
		return group
	end
	
	local function viewBomb(x, y, width)
	
		local group = display.newGroup()
		group.x, group.y = x, y
	
		local height = width / HALF_SQRT3
	
		local shadow = viewHexShadow( 0, 0, width )
		group:insert( shadow )
	
		local hex = display.newImageRect( group, "images/bomb_win.png", width, height )
		return group
	end
	
	local function viewExplosion(x, y, width)
	
		local group = display.newGroup()
		group.x, group.y = x, y
	
		local height = width / HALF_SQRT3
	
		local shadow = viewHexShadow( 0, 0, width )
		group:insert( shadow )
	
		local hex = display.newImageRect( group, "images/explosion.png", width, height )
		return group
	end
	
	local function viewFlag(x, y, width)
		
		local group = display.newGroup()
		group.x, group.y = x, y
	
		local height = width / HALF_SQRT3
		
		local bg = viewHidden( 0, 0, width )
		group:insert( bg )
		
		local hex = display.newImageRect( group, "images/buttons/flag.png", width*7/8, height*7/8 )
		
		return group
	end
	
	local function viewFlagWrong(x, y, width)
		
		local group = display.newGroup()
		group.x, group.y = x, y
	
		local height = width / HALF_SQRT3
		
		local bg = viewHidden( 0, 0, width )
		group:insert( bg )
		
		local hex = display.newImageRect( group, "images/buttons/flag.png", width*7/8, height*7/8 )
		hex:setFillColor( 1, 0, 0 )
		
		return group
	end
	
	-- public functions	
	function DRAW.hexToScreen(xh, yh)
		return hexToScreen(xh, yh, DRAW.origin, DRAW.scale)
	end
	
	function DRAW.screenToHex(xs, ys)
		return screenToHex(xs, ys, DRAW.origin, DRAW.scale)
	end
	
	function DRAW.cartesianToScreen(xh, yh)
		return cartesianToScreen(xh, yh, DRAW.origin, DRAW.scale)
	end
	
	function DRAW.screenToCartesian(xs, ys)
		return screenToCartesian(xs, ys, DRAW.origin, DRAW.scale)
	end
	
	function DRAW.createViewHidden( x, y )
		local xs, ys = hexToScreen(x, y, DRAW.origin, DRAW.scale)
		local view = viewHidden( xs, ys, DRAW.scale-2)
		viewGroup:insert(view)
		return view
	end
	
	function DRAW.createViewNumber( number, x, y )
		local xs, ys = hexToScreen(x, y, DRAW.origin, DRAW.scale)
		local view = viewNumber( xs, ys, DRAW.scale-2, number)
		viewGroup:insert(view)
		return view
	end
	
	function DRAW.createViewBomb( x, y )
		local xs, ys = hexToScreen(x, y, DRAW.origin, DRAW.scale)
		local view = viewBomb( xs, ys, DRAW.scale-2 )
		viewGroup:insert(view)
		return view
	end

	function DRAW.createViewExplosion( x, y )
		local xs, ys = hexToScreen(x, y, DRAW.origin, DRAW.scale)
		local view = viewExplosion( xs, ys, DRAW.scale-2 )
		viewGroup:insert(view)
		return view
	end
	
	function DRAW.createViewFlag( x, y )
		local xs, ys = hexToScreen(x, y, DRAW.origin, DRAW.scale)
		local view = viewFlag( xs, ys, DRAW.scale-2 )
		viewGroup:insert(view)
		return view
	end
	
	function DRAW.createViewFlagWrong( x, y )
		local xs, ys = hexToScreen(x, y, DRAW.origin, DRAW.scale)
		local view = viewFlagWrong( xs, ys, DRAW.scale-2 )
		viewGroup:insert(view)
		return view
	end
	
	function DRAW.destroyView( view )
		if view then
			display.remove( view )
		end
	end
	
	function DRAW.start(options)
		DRAW.scale = params[ difficulty ].scale
		DRAW.origin = params[ difficulty ].origin
	
		viewGroup = display.newGroup()
		if options and options.rootViewGroup then
			options.rootViewGroup:insert( viewGroup )
		end
	end
	
	function DRAW.stop()
		if viewGroup then
			display.remove( viewGroup )
		end
	end
end


---------------------------------------
local UI = {}
do -- UI

	local items = {}

	-- constants
	local screenW, screenH = display.contentWidth, display.contentHeight
	local centerX, centerY = screenW*0.5, screenH*0.5
	local errorY = (display.pixelHeight * display.contentScaleY) - display.contentHeight
	
	local function createRestartButton()
	
		-- on restart event
		local function onRestartButtonTap( event )
			if state ~= 'running' and state ~= 'ready' then
				GAME.restart()
			end
		end
		
		local restart = buttons.createImageButton{
			image = 'images/buttons/restart.png',
			onTap = onRestartButtonTap,
			x = screenW * 1/8,
			y = screenH* 1/12 - errorY/2,
			width = 50,
			height = 58,
			parent = group
		}
		return restart
	end
	
	local function createBackButton()
		
		-- on back event
		local function onBackButtonTap( event )
			storyboard.gotoScene( 'menuscene', {
				effect = "slideRight",
				time = "250"
			} )
		end
	
		local back = buttons.createImageButton{
			image = 'images/buttons/back.png',
			onTap = onBackButtonTap,
			x = screenW * 7/8,
			y = screenH* 1/12 - errorY/2,
			width = 50,
			height = 58,
			parent = group
		}
		return back
	end
	
	local function onFlagButtonTap( event )
		if state == 'running' then
			local btn = items['flag']
			flagMode = not flagMode
		end
	end
	
	local function createFlagButton()
	
		local group = display.newGroup()
		group.x = screenW * 4/8
		group.y = screenH * 1/12 - errorY/2
	
		local flag = buttons.createImageButton{
			image = 'images/buttons/flag.png',
			onTap = onFlagButtonTap,
			width = 60,
			height = 69,
			parent = group
		}
		
		local flag_set = buttons.createImageButton{
			image = 'images/buttons/flag_set.png',
			onTap = onFlagButtonTap,
			width = 60,
			height = 69,
			parent = group
		}
		-- TODO: hack, must change image
		flag_set:setFillColor( 0.90 )
	
		function group.setFlagMode( isPressed )
			flag.isVisible = not isPressed
			flag_set.isVisible = isPressed
		end
		
		group.setFlagMode( false )
		
		return group
	end
	
	function startAnimatingButton( btn )
		local anims = {}
		local animIdx = 1
		local function runNext(btn)
			anims[animIdx](btn)
			animIdx = 1 + ( animIdx % 2 )
		end
		anims[1] = function (btn)
			transition.to( btn, { 
				time = 100, 
				yScale = 0.95,
				xScale = 0.95,
				onComplete = function() runNext(btn) end
			} ) 
		end
		anims[2] = function (btn)
			transition.to( btn, { 
				time = 100, 
				yScale = 1.05,
				xScale = 1.05,
				onComplete = function() runNext(btn) end
			} ) 
		end
		runNext( btn )
	end
	
	function stopAnimatingButton( btn )
		transition.cancel( btn )
		btn.xScale, btn.yScale = 1, 1
	end
	
	local function createImage( opts )
		local group = display.newGroup() 
		group.x = opts.x
		group.y = opts.y

		local shadow = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		shadow.x, shadow.y = 1, 3 -- i, i
		shadow:setFillColor( 0, 0, 0 )
		shadow:toBack()
		shadow.alpha = 0.25
		
		local image = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		
		if opts.parent then
			opts.parent:insert( group )
		end
		return group
	end
	
	local function createYouWin()
	
		local img = createImage{
			image = 'images/you_win.png',
			x = centerX,
			y = screenH * 1/12 - errorY/2,
			width = 140,
			height = 30,
			parent = group
		}
		return img
	end
	
	local function createYouLose()
		local img = createImage{
			image = 'images/you_lose.png',
			x = centerX,
			y = screenH * 1/12 - errorY/2,
			width = 150,
			height = 30,
			parent = group
		}
		return img
	end
	
	local function createBottomBar()
		local group = display.newGroup()
		group.x = centerX
		group.y	= screenH - 50 + errorY /2
		group.anchorChildren = true
		group.anchorY = 1
		
		local rect = display.newRect(
			group, -- parent
			0, 0, -- x, y
			screenW + 40, -- for 4/3 screen ratio
			36
		)
		rect:setFillColor( unpack(colors.bottom_bar.fill) )
		
		local function createItens( parent, color )
			local group = display.newGroup()
			parent:insert( group )
			
			local timer = display.newText{
				parent = group,
				text = "00:00",
				font = native.systemFont,
				fontSize = 31
			}
			timer:setFillColor( unpack(color) )
			
			local bombs = display.newText{
				parent = group,
				text = "00",
				font = native.systemFont,
				fontSize = 26,
				x = screenW * 3/8 + 15
			}
			bombs:setFillColor( unpack(color) )
			
			local bomb_img = display.newImageRect(
				group, -- parent,
				'images/bomb.png', -- filename
				30, 30
			)
			bomb_img.x = screenW * 3/8 -20
			bomb_img:setFillColor( unpack(color) )
			
			local flags = display.newText{
				parent = group,
				text = "00",
				font = native.systemFont,
				fontSize = 26,
				x = screenW * -3/8+15
			}
			flags:setFillColor( unpack(color) )
			local flag_img = display.newImageRect(
				group, -- parent,
				'images/flag.png', -- filename
				30, 30
			)
			flag_img.x = screenW * -3/8 -20
			flag_img:setFillColor( unpack(color) )
			
			function group.setBombs( n ) 
				bombs.text = n
			end
			
			function group.setFlags( n ) 
				flags.text = n
			end
			
			function group.setTime( n ) 
				timer.text = n
			end
			
			return group
		end
		
		local textColor = colors.bottom_bar.text
		local shadowColor = colors.bottom_bar.textShadow
		
		local itemsShadow = createItens( group, shadowColor )
		itemsShadow.x, itemsShadow.y = 1, 1
		local items = createItens( group, textColor )

		-----------
		function group.setBombs( n ) 
			items.setBombs( n )
			itemsShadow.setBombs( n )
		end
		
		function group.setFlags( n ) 
			items.setFlags( n )
			itemsShadow.setFlags( n )
		end
		
		function group.setTime( n ) 
			items.setTime( n )
			itemsShadow.setTime( n )
		end
		
		return group
	end
	
	-- public functions
	function UI.update()

		do -- bombs and flags
			local nbombs = difficultyParams[difficulty].bombs
			-- count flags
			local nflags = 0
			for y, linha in pairs( hexes ) do
				for x, hex in pairs ( linha ) do
					if hex.state == 'flag' then
						nflags = nflags + 1
					end
				end
				if disclosedBomb then break end
			end
			
			local bottom = items['bottom']
			bottom.setBombs( nbombs )
			bottom.setFlags( nflags )
		end
		
		do -- set game time
			local timeInSec = gameTime / 1000
			local s = timeInSec % 60
			local m = timeInSec / 60
			local str = string.format("%02d:%02d", m, s)
			local bottom = items['bottom']
			bottom.setTime( str )
		end
		
		if state ~= UI.state then
			UI.state = state
			
			-- restart button
			local restart = items['restart']
			if UI.state ~= 'win' and UI.state ~= 'lose' then
				restart.alpha = 0.25
			else
				restart.alpha = 1
			end
			
			-- flag button
			local flag = items['flag']
			if UI.state == 'ready' then
				flag.isVisible = true
				flag.alpha = 0.25
			elseif UI.state == 'running' then
				flag.isVisible = true
				flag.alpha = 1
			else
				flag.isVisible = false
			end
			
			-- win/lose
			local youwin = items['youwin']
			local youlose = items['youlose']
			if UI.state ~= 'win' and UI.state ~= 'lose' then
				youwin.isVisible = false
				youlose.isVisible = false
			elseif UI.state == 'win' then
				youwin.isVisible = true
				youlose.isVisible = false
			elseif UI.state == 'lose' then
				youwin.isVisible = false
				youlose.isVisible = true
			end
			
		end
	
		if flagMode ~= UI.flagMode then
			UI.flagMode = flagMode
		
			local btn = items['flag']
			btn.setFlagMode( flagMode )
			--[[ TODO: to remove or not to remove ]]
			if flagMode then
				startAnimatingButton( btn )
			else
				stopAnimatingButton( btn )
			end 
		end
	end
	
	function UI.start( options )
		UI.flagMode = flagMode or false
		UI.state = 'invalid'
		
		items['restart'] = createRestartButton()
		items['back'] = createBackButton()
		items['flag'] = createFlagButton()
		items['youwin'] = createYouWin()
		items['youlose'] = createYouLose()
		items['bottom'] = createBottomBar()
		
		if options and options.rootViewGroup then
			local group = options.rootViewGroup
			for i, item in pairs( items ) do
				group:insert( item )
			end
		end
	end
	
	function UI.stop()
		for i, item in pairs( items ) do
			display.remove( item )
		end
		items = {}
	end
end

---------------------------------------
local INPUT = {}
do -- INPUT

	-- consts
	local DEBUG = false
	
	-- private vars
	local tapGroup = nil
	local tappedHex = nil
	
	-- private functions
	local function onHexTap( event )
		-- uses view position to find hex instead of event position
		local xh, yh = DRAW.screenToHex( event.target.x, event.target.y)
		-- eliminate small round errors
		xh, yh = math.floor(xh+0.5), math.floor(yh+0.5)
		tappedHex = hexes[yh][xh]
	end
	
	-- public functions
	function INPUT.createTapInputForHex( x, y )
		local xs, ys = DRAW.hexToScreen(x, y)
		
		local circle = display.newCircle(xs, ys, (DRAW.scale/2) * 0.95)
		if(DEBUG) then
			circle.strokeWidth = 1
			circle:setStrokeColor(  1, 0, 0 )
			circle:setFillColor( 0.5, 1, 0.5, 0.25 )
			tapGroup:toFront()
		else 
			circle.isVisible = false
			circle.isHitTestable = true
		end
		tapGroup:insert(circle)
		circle:addEventListener( "tap", onHexTap )
	end
	
	function INPUT.destroyTapInput( tapInput )
		if tapInput then
			display.remove( tapInput )
		end
	end
	
	function INPUT.update()
		local hex = tappedHex
		tappedHex = nil
		if hex then
			selectedHex = hex
		end
	end	
	
	function INPUT.start() 
		tappedHex = nil
		tapGroup = display.newGroup()
		if options and options.rootViewGroup then
			options.rootViewGroup:insert( tapGroup )
		end
	end
	
	function INPUT.stop() 
		tappedHex = nil
		if tapGroup then
			display.remove( tapGroup )
		end
	end
	
end

---------------------------------------
local GAME_RULES = {}
do 
	local count = 0
	
	local function discloseHex( hex, openOnZeroes )
		local x, y = hex.position.x, hex.position.y
		if hex.state == 'hidden' then
			hex.state = 'open'
			hex.oldView = hex.view
			--DRAW.destroyView( hex.view ) -- replaced by animation
			
			if hex.type == 'number' then
				
				if hex.number == 0 and openOnZeroes then
					local locations = hexploderBoard.RegionGet( templateBoard, { x=x, y=y } )
					for i, location in pairs( locations ) do
						local hex2 = hexes[location.y][location.x]
						if hex2.state == 'hidden' then
							hex2.state = 'open'
							hex2.oldView = hex2.view
							--DRAW.destroyView( hex2.view ) -- replaced by animation
							hex2.view = DRAW.createViewNumber( hex2.number, location.x, location.y )
							
							transition.from( hex2.view, { 
								time = 150, alpha = 0,
								onComplete = function() 
									DRAW.destroyView( hex2.oldView )
									hex2.oldView = nil
								end
							} )
						end
					end 
				end
				
				hex.view = DRAW.createViewNumber( hex.number, x, y )
			else
				if state == 'win' then
					hex.view = DRAW.createViewBomb( x, y )
				else
					hex.view = DRAW.createViewExplosion( x, y )
				end
			end
			
			transition.from( hex.view, { 
				time = 150, alpha = 0,
				onComplete = function() 
					DRAW.destroyView( hex.oldView )
					hex.oldView = nil
				end
			} )
		end
	end
	
	local function flagHex( hex )
		local x, y = hex.position.x, hex.position.y
		if hex.state == 'hidden' then
			hex.state = 'flag'
			DRAW.destroyView( hex.view )
			hex.view = DRAW.createViewFlag( x, y )
		elseif hex.state == 'flag' then
			hex.state = 'hidden'
			DRAW.destroyView( hex.view )
			hex.view = DRAW.createViewHidden( x, y )
		end
	end
	
	local function loadBombs( data )
		for y, linha in pairs( hexes ) do
			for x, hex in pairs ( linha ) do
				if data[y][x] == 'b' then
					hex.type = 'bomb'
				else
					hex.type = 'number'
					hex.number = data[y][x]
				end
			end
		end
	end
	
	function GAME_RULES.update( delta )
		
		if state == 'ready' and selectedHex then
			state = 'running'
			
			-- generate bombs
			local p = difficultyParams[ difficulty ]
			local hb = templateBoard
			hexploderBoard.GenerateBombs( hb, p.bombs )
			
			local pos = selectedHex.position
			while hb.board[pos.y][pos.x] == 'b' do
				hb = hexploderBoard.New( hb.width, hb.height )
				hexploderBoard.GenerateBombs( hb, p.bombs )
				templateBoard = hb
			end
			loadBombs( hb.board )
			
		end
		
		if state == 'running' then
		
			-- increment time
			gameTime = gameTime + delta
		
			-- process command 
			if selectedHex then
				if flagMode then
					flagHex( selectedHex )
				else
					discloseHex( selectedHex, true )
				end
				selectedHex = nil
			end
			
			-- compute losing condition
			do
				local disclosedBomb = nil
				-- search for disclosed bombs
				for y, linha in pairs( hexes ) do
					for x, hex in pairs ( linha ) do
						if hex.state == 'open' and hex.type == 'bomb' then
							disclosedBomb = hex
						end
					end
					if disclosedBomb then break end
				end
				-- if player disclosed a bomb it loses the game
				if disclosedBomb then
					state = 'lose'
					-- open all hexes
					for y, linha in pairs( hexes ) do
						for x, hex in pairs ( linha ) do
							discloseHex( hex )
						end
					end
					-- display wrong flags
					for y, linha in pairs( hexes ) do
						for x, hex in pairs ( linha ) do
							if hex.state == 'flag'
								and hex.type ~= 'bomb' 
							then
								DRAW.destroyView( hex.view )
								hex.view = DRAW.createViewFlagWrong( x, y )
							end
						end
					end
					return
				end
			end
			
			-- compute winning condition
			if state ~= 'lose' then
				local opened = 0
				local total = 0
				for y, line in pairs (hexes) do
					for x, hex in pairs (line) do
						total = total + 1
						if hex.state == 'open' then
							opened = opened + 1
						end
					end
				end

				-- The user wins when the number of opened spaces is equal to (total spaces - bombs)
				if opened == total - difficultyParams[difficulty].bombs then
					state = 'win'
					-- open all hexes
					for y, linha in pairs (hexes) do
						for x, hex in pairs (linha) do
							if hex.state == 'hidden' then
								discloseHex (hex)
							end
						end
					end
					return
				end
			end
			
		end
		
	end
	
	function GAME_RULES.start()
	end
	
	function GAME_RULES.stop()
	end
	
end

---------------------------------------
do -- GAME

	GAME = {
		frame = 0,
		time = 0,
		delta = 0,
		options = nil
	}
	
	-- private functions
	local function onEnterFrame( event )
		if GAME.frame == 0 then
			GAME.delta = 0
			GAME.time = event.time
		end
		
		GAME.delta = event.time - GAME.time
		GAME.time = event.time
		GAME.frame = GAME.frame + 1
		
		GAME.update( GAME.delta )
	end
	
	local function loadMap(width, height, data)
	
		for x = 1, width do
			for y = 1, height do
				if data[y][x] ~= 'x' then
					local hex = {}
					hex.position = { x=x, y=y }
					hex.tapInput = INPUT.createTapInputForHex( x, y )
					
					-- default hex are hidden
					hex.state = 'hidden' -- 'open', 'flaged'
					-- set the default view (hidden)
					hex.view = DRAW.createViewHidden( x, y )
					
					-- place the hex indexed by its position
					hexes[y] = hexes[y] or {}
					hexes[y][x] = hex
				end
			end
		end
	end
	
	local function loadResumeData( resumeData )
		
		state = resumeData.state
		gameTime = resumeData.time
		templateBoard = resumeData.templateBoard
		
		local saved = resumeData.hexes
		
		for y, line in pairs( saved ) do
			for x, cell in pairs( line ) do
				local hex = {}
				hex.position = { x=x, y=y }
				hex.tapInput = INPUT.createTapInputForHex( x, y )
				
				hex.type = cell.type
				hex.state = cell.state
				hex.number = cell.number
				
				if hex.state == 'hidden' then
					hex.view = DRAW.createViewHidden( x, y )
				elseif hex.state == 'flag' then
					hex.view = DRAW.createViewFlag( x, y )
				else
					if hex.type == 'number' then
						hex.view = DRAW.createViewNumber( hex.number, x, y )
					else 
						hex.view = DRAW.createViewBomb( x, y )
					end
				end
				
				-- place the hex indexed by its position
				hexes[y] = hexes[y] or {}
				hexes[y][x] = hex
			end
		end
		
	end
	
	local function loadLevel( options, resumeData )
		
		local p = difficultyParams[ difficulty ]
		
		if resumeData then
			loadResumeData( resumeData )
		else
			local hb = hexploderBoard.New( p.width, p.height )
			--hexploderBoard.GenerateBombs( hb, p.bombs )
			templateBoard = hb
			loadMap( hb.width, hb.height, hb.board )
		end
	end
	
	-- public functions
	
	function GAME.start( options, resumeData )
		options = options or {}
		GAME.options = options
	
		-- delete gameData
		hexes = {}
		difficulty = options.difficulty
		selectedHex = nil
		flagMode = false
		state = 'ready'
		gameTime = 0
		
		-- game time vars
		GAME.frame = 0
		GAME.delta = 0
		GAME.time = 0
	
		-- initialize systems
		DRAW.start( options )
		GAME_RULES.start( options )
		INPUT.start( options )
		UI.start( options )
				
		-- initialize game data
		loadLevel( options, resumeData )
		
		-- start the game loop
		Runtime:addEventListener( "enterFrame", onEnterFrame )
	end
	
	function GAME.stop()
		-- stop the game loop
		Runtime:removeEventListener( "enterFrame", onEnterFrame )
		
		-- shutdown systems
		DRAW.stop()
		GAME_RULES.stop()
		INPUT.stop()
		UI.stop()
	end
	
	function GAME.update( delta )
		UI.update( delta )
		INPUT.update( delta )
		GAME_RULES.update( delta )
	end
	
	function GAME.restart()
		GAME.stop()
		GAME.start( GAME.options )
	end
	
	function GAME.getSaveData()
	
		local saveTable = {}
		-- 
		for y, linha in pairs( hexes ) do
			saveTable[y] = {}
			for x, hex in pairs ( linha ) do
				saveTable[y][x] = {}
				saveTable[y][x].state = hex.state
				saveTable[y][x].type = hex.type
				saveTable[y][x].number = hex.number
			end
		end
		
		return {
			templateBoard = templateBoard,
			hexes = saveTable,
			state = state,
			time = gameTime,
			difficulty = difficulty
		}
	end
	
	function GAME.startWithSaveData( data, options )
		options.difficulty = data.difficulty
		
		GAME.start( options, data )
	end
	
	function GAME.isRunning()
		return state == 'running'
	end
	
end

return GAME