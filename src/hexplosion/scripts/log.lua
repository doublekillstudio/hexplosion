-------------------------------------------------------------------------------
--
-- debug.lua
--
-------------------------------------------------------------------------------

local log = {}
do
	local DEBUG = false

	if DEBUG then -- enable log
	
		local FONT_SIZE = 12
		local MAX_LINES = 34
	
		local shadowText = display.newText {
			text = "DEBUG > ", 
			x = 2, y = 2,
			font = native.systemFont, 
			fontSize = FONT_SIZE
		}
		shadowText.anchorX = 0
		shadowText.anchorY = 0
		shadowText:setFillColor( 1 )
		shadowText.alpha = 0.7
	
		local logText = display.newText {
			text = "DEBUG > ", 
			x = 0, y = 0,
			font = native.systemFont, 
			fontSize = FONT_SIZE
		}
		logText.anchorX = 0
		logText.anchorY = 0
		logText:setFillColor( 0 )
		logText.alpha = 0.9
	
		function log.debug( format, ... )
			
			local msg = format and string.format( format, ... ) or '<nil>'
			-- print message
			print( 'DEBUG>', msg )
			
			-- display message in screen
			local str = logText.text
			str = str .. '\n' .. msg
			
			local n = select(2, string.gsub( str, '\n', '\n' ) ) + 1
			if n > MAX_LINES then
				str = string.gsub( str, '^' .. string.rep( '[^\n]*\n', n-MAX_LINES ), '' )
			end
			
			shadowText.text = str
			shadowText:toFront()
			logText.text = str
			logText:toFront()
			
		end
	
	else -- disable log
		function log.debug() end
	end
	
end
return log