-------------------------------------------------------------------------------
--
-- savegame.lua
--
-------------------------------------------------------------------------------

local json = require("json")

local savegame = {}
do
	local DATA = nil

	local filename = 'resume.save'
	
	-- function to save a table as a savegame
	function savegame.save(t)
		DATA = t
		--[[
		local path = system.pathForFile( filename, system.DocumentsDirectory)
		local file = io.open(path, "w")
		if file then
			local contents = json.encode(t)
			file:write( contents )
			io.close( file )
			return true
		else
			return false
		end
		]]
	end
	
	function savegame.load()
		return DATA
		--[[
		local path = system.pathForFile( filename, system.DocumentsDirectory)
		local contents = ""
		local myTable = {}
		local file = io.open( path, "r" )
		if file then
			 -- read all contents of file into a string
			 local contents = file:read( "*a" )
			 myTable = json.decode(contents);
			 io.close( file )
			 return myTable 
		end
		return nil
		]]
	end
	
	function savegame.exists()
		return (DATA ~= nil)
		--[[
		local path = system.pathForFile( filename, system.DocumentsDirectory)
		local file = io.open( path, "r" )
		if file then
			io.close( file )
			return true 
		end
		return false
		]]
	end
	
	function savegame.delete()
		DATA = nil
		--[[
		local path = system.pathForFile( filename, system.DocumentsDirectory)
		return os.remove( path )
		]]
	end
end
return savegame