-------------------------------------------------------------------------------
--
-- difficulties.lua
--
-------------------------------------------------------------------------------

-- requires
local storyboard = require( "storyboard" )
local widget = require( "widget" )

local colors = require( "scripts.colors" ) 
local buttons = require( "scripts.buttons" )
local log = require( "scripts.log" )

-- consts
local screenW, screenH = display.contentWidth, display.contentHeight
local centerX, centerY = screenW*0.5, screenH*0.5
local errorY = (display.pixelHeight - (display.contentHeight / display.contentScaleY)) / 2
local HALF_SQRT3 = math.sqrt(3)/2

-- menuscene scene	
local scene
do
	scene = storyboard.newScene( 'difficulties' )

	-- go to game with selected difficulty
	local function gotoGameScene( difficulty )
		log.debug( 'dificuldade = %s', difficulty )
		storyboard.gotoScene( 'gamescene', {
			effect = "slideLeft",
			time = "250",
			params = {
				difficulty = difficulty or 'medium',
				resume = false
			}
		} )
	end
	
	-- functions to handle button events
	local function easyBtnRelease( event )
		 gotoGameScene( 'easy' )
	end
	local function mediumBtnRelease( event )
		 gotoGameScene( 'medium' )
	end
	local function hardBtnRelease( event )
		 gotoGameScene( 'hard' )
	end

	-- functions to handle button events
	local function backBtnRelease( event )
		storyboard.gotoScene( 'menuscene', {
			effect = "slideRight",
			time = "250"
		} )
	end

	-- function to create logo
	local function createLogo( opts )
		local group = display.newGroup() 
		group.x = opts.x
		group.y = opts.y

		local shadow = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		shadow.x, shadow.y = 1, 3 -- i, i
		shadow:setFillColor( 0, 0, 0 )
		shadow:toBack()
		shadow.alpha = 0.25
		
		local image = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		
		if opts.parent then
			opts.parent:insert( group )
		end
		return group
	end
	
	-- scene's view is created
	function scene:createScene( event )	
		local group = self.view
		
		local logo = createLogo{
			image = 'images/logo.png',
			x = centerX,
			y = screenH * 2/8,
			width = 260,
			height = 115,
			parent = group
		}
		
		--[[
		local rect = display.newRect(
			group, -- parent
			centerX, -- x
			screenH/16 * 4, -- y
			display.pixelWidth * display.contentScaleX, -- for 4/3 screen ratio
			42
		)
		rect:setFillColor( 0 )
		
		local logo = createLogo{
			image = 'images/menu/new_game.png',
			x = centerX,
			y = screenH/16 * 4,
			width = 210,
			height = 36,
			parent = group
		}
		]]
		
		local easy = buttons.createImageButton{
			image = 'images/menu/easy.png',
			width = 98,
			height = 36,
			onTap = easyBtnRelease,
			x = centerX,
			y = screenH/8 * 4,
			parent = group,
		}
		
		local medium = buttons.createImageButton{
			image = 'images/menu/medium.png',
			width = 156,
			height = 36,
			onTap = mediumBtnRelease,
			x = centerX,
			y = screenH/8 * 5,
			parent = group,
		}
		
		local hard = buttons.createImageButton{
			image = 'images/menu/hard.png',
			width = 104,
			height = 36,
			onTap = hardBtnRelease,
			x = centerX,
			y = screenH/8 * 6,
			parent = group,
		}
		
		local back = buttons.createImageButton{
			image = 'images/buttons/back.png',
			width = 50,
			height = 58,
			onTap = backBtnRelease,
			x = screenW * 7/8,
			y = screenH* 1/12 - errorY/2,
			parent = group
		}
		
	end
	
	-- bind events
	scene:addEventListener( "createScene", scene )
end
return scene