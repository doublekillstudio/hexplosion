-------------------------------------------------------------------------------
--
-- gamescene.lua
--
-------------------------------------------------------------------------------

-- requires
local storyboard = require( "storyboard" )

local game = require( "scripts.game" )
local log = require( "scripts.log" )
local savegame = require('scripts.savegame')
local ads = require ('scripts.ads')

-- consts
local HALF_SQRT3 = math.sqrt(3)/2

-- splashscreen scene	
local scene
do
	scene = storyboard.newScene( "gamescene" )
	
	-- SAVE AND LOAD GAME
	do	
		
	end

	-- Called immediately after scene has moved onscreen:
	function scene:willEnterScene( event )
		-- If the current add is interstitial, cancel the ad and generate a banner
		if ads.currentType == 'interstitial' then
			ads.hide ()
			ads.show ('banner')
		end
		
		local options = {
			rootViewGroup = scene.view
		}
		
		local resumedWithSuccess = false
		
		if event.params and event.params.resume then
			local data = savegame.load()
			if data then
				-- always delete any resume session
				savegame.delete()
				-- resume game
				game.startWithSaveData( data, options )
				resumedWithSuccess = true
			end
		end
		
		if not resumedWithSuccess then
			-- always delete any resume session
			savegame.delete()
			
			-- start a new game
			local difficulty = 'medium'
			if event.params and event.params.difficulty then
				difficulty = event.params.difficulty
			end
			options.difficulty = difficulty
			game.start( options )
		end
		
	end
	
	-- scene's view is created
	function scene:exitScene( event )
		if game.isRunning() then
			savegame.save( game.getSaveData() )
		end
		game.stop()

		-- When exiting the game, eventually try to exchange the banner for an interstitial
		if ads.canShowInterstititalAd () then
			ads.hide ()
			ads.show ('interstitial')
		end
	end
	
	-- bind events
	scene:addEventListener( "willEnterScene", scene )
	scene:addEventListener( "exitScene", scene )
	
end
return scene