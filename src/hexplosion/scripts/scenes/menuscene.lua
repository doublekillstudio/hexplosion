-------------------------------------------------------------------------------
--
-- menuscene.lua
--
-------------------------------------------------------------------------------

-- requires
local storyboard = require( "storyboard" )
local widget = require( "widget" )

local colors = require( "scripts.colors" ) 
local buttons = require( "scripts.buttons" )
local log = require( "scripts.log" )
local savegame = require( "scripts.savegame" )
local ads = require ('scripts.ads')

-- consts
local screenW, screenH = display.contentWidth, display.contentHeight
local centerX, centerY = screenW*0.5, screenH*0.5
local HALF_SQRT3 = math.sqrt(3)/2

-- menuscene scene	
local scene
do
	-- ............................................................................................

	scene = storyboard.newScene( 'menuscene' )

	-- functions to handle button events
	local function newGameBtnRelease( event )
		storyboard.gotoScene( 'difficulties', {
			--effect = "slideLeft",
			effect = "slideLeft",
			time = "250"
		} )
	end

	local function tutorialBtnRelease( event )
		storyboard.gotoScene( 'tutorialscene', {
			--effect = "slideLeft",
			effect = "slideLeft",
			time = "250"
		} )
	end
	
	local function resumeBtnRelease( event )
		if not event.target.enabled then 
			return 
		end
		
		storyboard.gotoScene( 'gamescene', {
			effect = "slideLeft",
			time = "250",
			params = { resume = true }
		} )
	end
	
	
	local function createLogo( opts )
		local group = display.newGroup() 
		group.x = opts.x
		group.y = opts.y

		local shadow = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		shadow.x, shadow.y = 1, 3 -- i, i
		shadow:setFillColor( 0, 0, 0 )
		shadow:toBack()
		shadow.alpha = 0.25
		
		local image = display.newImageRect(
			group, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		
		if opts.parent then
			opts.parent:insert( group )
		end
		return group
	end
	
	local resume = nil
	-- scene's view is created
	function scene:createScene( event )	
		local group = self.view
		
		local logo = createLogo{
			image = 'images/logo.png',
			x = centerX,
			y = screenH * 2/8,
			width = 260,
			height = 115,
			parent = group
		}
		
		local newGame = buttons.createImageButton{
			image = 'images/menu/new_game.png',
			width = 209,
			height = 36,
			x = centerX,
			y = screenH/8 * 4,
			parent = group,
			onTap = newGameBtnRelease,
		}
		
		resume = buttons.createImageButton{
			image = 'images/menu/resume.png',
			width = 158,
			height = 36,
			x = centerX,
			y = screenH/8 * 5,
			parent = group,
			onTap = resumeBtnRelease,
		}
		
		local tutorial = buttons.createImageButton{
			image = 'images/menu/tutorial.png',
			width = 181,
			height = 36,
			x = centerX,
			y = screenH/8 * 6,
			parent = group,
			onTap = tutorialBtnRelease,
		}

		-- If this is the first time, show a banner
		if ads.currentType == nil then
			ads.show ('banner')
		end
	end

	function scene:willEnterScene( event )
		if savegame.exists() then
			resume.alpha = 1
			resume.enabled = true
		else
			resume.alpha = 0.33
			resume.enabled = false
		end
	end
	
	-- bind events
	scene:addEventListener( "createScene", scene )
	scene:addEventListener( "willEnterScene", scene )
end
return scene