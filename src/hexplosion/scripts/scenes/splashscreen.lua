-------------------------------------------------------------------------------
--
-- splashscreen.lua
--
-------------------------------------------------------------------------------

-- requires
local storyboard = require( "storyboard" )

-- consts
local screenW, screenH = display.contentWidth, display.contentHeight
local centerX, centerY = screenW*0.5, screenH*0.5
local HALF_SQRT3 = math.sqrt(3)/2

-- splashscreen scene	
local scene
do
	scene = storyboard.newScene( 'splashscreen' )

	local function drawFlag(x, y, width)
		local hexborder_color = { 0.3, 0.5, 0.8 }
		local hexfill_color = { 0.45, 0.65, 0.95 }
		
		local group = display.newGroup()
		group.x, group.y = x, y
	
		local height = width / HALF_SQRT3
		local hexborder = display.newImageRect( group, "images/hex.png", width, height )
		hexborder:setFillColor( unpack( hexborder_color) )
		
		local hexfill = display.newImageRect( group, "images/hex.png", width*12/16, height*12/16 )
		hexfill:setFillColor( unpack( hexfill_color) )
		
		local flag = display.newImageRect( group, "images/flag-256.png", width*5/8, width*5/8 )
		flag.x = width * -0.05 
		
		return group
	end
	
	-- scene's view is created
	function scene:createScene( event )
		local group = self.view
		local width = screenW * 0.5
		local image = drawFlag( centerX, centerY, width )
		group:insert( image )
	end
	
	-- scene has moved onscreen
	local t = nil
	function scene:enterScene( event )
		t = timer.performWithDelay( 2000, 
			function() 
				storyboard.gotoScene( 'menuscene', {
					effect = "fade",
					time = "500"
				} )
			end
		)
	end
	
	-- bind events
	scene:addEventListener( "createScene", scene )
	scene:addEventListener( "enterScene", scene )
end
return scene