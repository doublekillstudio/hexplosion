-------------------------------------------------------------------------------
--
-- tutorialscene.lua
--
-------------------------------------------------------------------------------

-- requires
local storyboard = require( "storyboard" )

local colors = require( "scripts.colors" ) 
local buttons = require( "scripts.buttons" )
local log = require( "scripts.log" )

-- consts
local screenW, screenH = display.contentWidth, display.contentHeight
local centerX, centerY = screenW*0.5, screenH*0.5
local errorY = (display.pixelHeight - (display.contentHeight / display.contentScaleY)) / 2
local HALF_SQRT3 = math.sqrt(3)/2

-- menuscene scene	
local scene
do
	scene = storyboard.newScene( 'tutorialscene' )
	
	-- buttons
	local next
	local prev
	
	local MAX_PAGES = 5
	local pages = nil
	local currPage = 0
	
	-- functions to handle button events
	local function backBtnRelease( event )
		storyboard.gotoScene( 'menuscene', {
			effect = "slideRight",
			time = "250"
		} )
	end

	local function updateButtons()
		if currPage == 1 then
			prev.alpha = 0.25
		else
			prev.alpha = 1
		end
		if currPage == MAX_PAGES then
			next.alpha = 0.25
		else
			next.alpha = 1
		end
	end
	
		-- functions to handle button events
	local function nextBtnRelease( event )
		if currPage < MAX_PAGES then
			pages[currPage].isVisible = false
			currPage = currPage + 1
			pages[currPage].isVisible = true
		end
		updateButtons()		
	end
	
		-- functions to handle button events
	local function prevBtnRelease( event )
		if currPage > 1 then
			pages[currPage].isVisible = false
			currPage = currPage - 1
			pages[currPage].isVisible = true
		end
		updateButtons()
	end
	
	-- function to create logo
	local function displayTutorialImage( opts )
		local image = display.newImageRect(
			opts.parent, -- parent,
			opts.image, -- filename
			opts.width, opts.height
		)
		image.x = opts.x
		image.y = opts.y
		
		return image
	end
	
	-- scene's view is created
	function scene:createScene( event )	
		local group = self.view
		
		pages = {}
		for i = 1, MAX_PAGES do
			-- carrega imagens
			local page = displayTutorialImage{
				parent = group,
				image = 'images/tutorial/tutorial' .. i .. '.png',
				x = centerX,
				y = (screenH - errorY/2) * 4/8,
				width = 320,
				height = 320,
				parent = group
			}
			page.isVisible = false
			pages[i] = page
		end
		
		local back = buttons.createImageButton{
			image = 'images/buttons/back.png',
			width = 50,
			height = 58,
			onTap = backBtnRelease,
			x = screenW * 7/8,
			y = screenH* 1/12 - errorY/2,
			parent = group
		}
		
		local Ybtns = screenH* 11/12 + errorY/2 - 50
		
		next = buttons.createImageButton{
			image = 'images/buttons/next.png',
			width = 50,
			height = 58,
			onTap = nextBtnRelease,
			x = screenW * 7/8,
			y = Ybtns,
			parent = group
		}
		
		prev = buttons.createImageButton{
			image = 'images/buttons/previous.png',
			width = 50,
			height = 58,
			onTap = prevBtnRelease,
			x = screenW * 1/8,
			y = Ybtns,
			parent = group
		}
		
	end
	
	-- Called immediately after scene has moved onscreen:
	function scene:willEnterScene( event )
		for i = 1, MAX_PAGES do
			pages[i].isVisible = false
		end
		currPage = 1
		pages[1].isVisible = true
		updateButtons()
	end
	
	-- bind events
	scene:addEventListener( "createScene", scene )
	scene:addEventListener( "willEnterScene", scene )
end
return scene